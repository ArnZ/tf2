"Resource/UI/HudDamageAccount.res"
{
	"CDamageAccountPanel"
	{
		"fieldName"				"CDamageAccountPanel"
		"text_x"				"0"
		"text_y"				"0"
		"delta_item_end_y"		"0"
		"PositiveColor"			"75 175 255 255"
		"NegativeColor"			"144 223 0 255"
		"delta_lifetime"		"1.5"
		"delta_item_font"		"knFontDamage"
		"delta_item_font_big"		"knFontDamageBig"
	}
	"DamageAccountValue"
	{

		"ControlName"	"ceXLabel"
		"fieldName"		"DamageAccountValue"
		"xpos"			"c-130"
		"ypos"			"c55"
		"zpos"			"2"
		"wide"			"150"
		"tall"			"26"
		"visible"		"1"
		"enabled"		"1"
		"labelText"		"%metal%"
		"textAlignment"	"center"
		"font"			"knFontDemiBold11"
		"fgcolor_override"			"144 223 0 255" // "144 223 0 255"
	}
	"DamageAccountValueShadow"
	{
		
		"ControlName"	"ceXLabel"
		"fieldName"		"DamageAccountValueShadow"
		"xpos"			"c-129"
		"ypos"			"c56"
		"zpos"			"2"
		"wide"			"150"
		"tall"			"26"
		"visible"		"1"
		"enabled"		"1"
		"labelText"		"%metal%"
		"textAlignment"	"center"
		"font"			"knFontDemiBold11"
		"fgcolor_override"			"0 0 0 255"		//black
	}
}