"Resource/UI/HudDamageAccount.res"
{
	"CDamageAccountPanel"
	{
		"fieldName"				"CDamageAccountPanel"
		"text_x"				"0"
		"text_y"				"0"
		"delta_item_end_y"		"0"
		"PositiveColor"			"255 255 255 255"
		"NegativeColor"			"255 255 255 255"
		"delta_lifetime"		"2"
		"delta_item_font"		"HudFontMediumSmallBold"
		"delta_item_font_big"	"HudFontMediumSmallBold"
	}

"DamageAccountValue"
	{
		"fgcolor"	"255 255 0 255"
		"ControlName"	"ceXLabel"
		"fieldName"	"DamageAccountValue"
		"xpos"		"c-78"
		"xpos_minmode"	"c-73"
		"ypos"		"c138"
		"ypos_minmode"	"c80"
		"zpos"		"1"
		"wide"		"150"
		"tall"		"26"
		"visible"	"1"
		"enabled"	"1"
		"labelText"	"%metal%"
		"textAlignment"	"center"
		"font"		"HudFontMediumSmallBold"	
		"font_minmode"	"HudFontMediumSmallBold"
	}

}